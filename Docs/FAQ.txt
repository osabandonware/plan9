URL: http://www.fywss.com/plan9/plan9faq.html

This document answers frequently asked questions about the third edition of
the Plan 9 operating system.

The following sections are new or modified recently:

   * Where can I get more Plan 9 software?
   * Can I emulate Plan 9 under Unix?
   * How do I cut and paste with a 2 button mouse?

A hypertext version of this FAQ is available on my Plan 9 web page, URL
http://www.fywss.com/plan9/

Other sources of information include the newsgroup comp.os.plan9, which is
bidirectionally gatewayed to the 9fans mailing list (browse archives at
https://lists.cse.psu.edu/archives/9fans/ and
http://bio.cse.psu.edu/~schwartz/9fans/, or mail 9fans-request@cse.psu.edu
to subscribe) and of course the Plan 9 homepage at Bell Labs, URL
http://plan9.bell-labs.com/plan9dist/

If you'd like to discuss the Plan9 license, send mail to
plan9-license-discussions@plan9.bell-labs.com. Mailing to this list
subscribes you to the list.

Please forward any comments or suggestions regarding this FAQ to
steve@fywss.com
  ------------------------------------------------------------------------

Introduction:

   * What is Plan 9?
   * What is in the latest Plan9 release?
   * What is its relation to other operating systems?
   * What are its key ideas?
   * What are the advantages to this approach?

Hardware and Software:

   * What platforms does it run on?
   * Is anyone working on a port for my system?
   * Does it support symmetric multiprocessing?
   * What about applications and tools?
   * Is there a fortran compiler?
   * Where can I get more Plan 9 software?
   * Is it object-oriented?
   * What about application portability?
   * What resources does it need?
   * What GUIs does it support?
   * How do I cut and paste with a 2 button mouse?
   * Does Plan 9 have any Unix-like terminal emulators?
   * What character set does it use?
   * What about security and user authentication?
   * How does it communicate with other systems?
   * Is it suitable for real time control?

Installation and Administration:

   * What PC hardware works well with Plan 9?
   * How do I Install Plan 9?
   * It doesn't work for me, how should I troubleshoot?
   * How do I setup the VGA?
   * How do I control the services that start at boot time?
   * How do I setup network services?
   * How do I shutdown my terminal/cpuserver system?
   * How do I reboot my system?

General Information:

   * Where did the name come from?
   * How can I Obtain Plan 9?
   * How can I get involved?
   * Where can I get more detailed technical information?
   * Can I emulate Plan 9 under Unix?
   * Is the cross product of two vectors a vector?
   * Are there any Plan 9 user groups?

  ------------------------------------------------------------------------

Introduction:

Subject: What is Plan 9?

Plan 9 is a new computer operating system and associated utilities. It was
built by the Computing Science Research Center of Lucent Technologies Bell
Laboratories, the same group that developed Unix, C, and C++.

Plan 9 is a distributed system. In the most general configuration, it uses
three kinds of components: terminals that sit on users' desks, file servers
that store permanent data, and other servers that provide faster CPUs, user
authentication, and network gateways. These components are connected by
various kinds of networks, including Ethernet, specially-built fiber
networks, ordinary modem connections, and ISDN. In typical use, users
interact with applications that run either on their terminals or on CPU
servers, and the applications get their data from the file servers. The
design, however, is highly configurable; it escapes from specific models of
networked workstations and central machine service.

Subject: What is in the latest Plan9 release?

The press release for the third edition of Plan9 is at
http://www.bell-labs.com/news/2000/june/7/2.html

Among the changes in this release of Plan 9 are a revised kernel, which now
has the means to resolve ambiguous file names; an improved graphics
environment; an updated command set; and expanded libraries. The system's
creators also have installed "plumbing," a new mechanism for passing
messages between interactive programs, as part of the user interface.

The new release is available for free download under an open source
agreement. This is a significant step over previous releases.

If you'd like to browse the distribution packages before actually
installing, you'll need the tools available at
http://www.fywss.com/plan9/unix/

Subject: For History Buffs

The first edition of Plan 9 was released in 1993, and was only available to
universities.

In 1995 the second edition was available for purchase under a shrink-wrap
license.

The second edition version of this FAQ is archived at
http://www.fywss.com/plan9/plan9v2faq.html

Subject: What is its relation to other operating systems?

Plan 9 is itself an operating system; it doesn't run as an application under
another system. It was written from the ground up and doesn't include other
people's code. Although the OS's interface to applications is strongly
influenced by the approach of Unix, it's not a replacement for Unix; it is a
new design.

Subject: What are its key ideas?

Plan 9 exploits, as far as possible, three basic technical ideas: first, all
the system objects present themselves as named files that are manipulated by
read/write operations; second, all these files may exist either locally or
remotely, and respond to a standard protocol; third, the file system name
space - the set of objects visible to a program - is dynamically and
individually adjustable for each of the programs running on a particular
machine. The first two of these ideas were foreshadowed in Unix and to a
lesser extent in other systems, while the third is new: it allows a new
engineering solution to the problems of distributed computing and graphics.
Plan 9's approach means that application programs don't need to know where
they are running; where, and on what kind of machine, to run a Plan 9
program is an economic decision that doesn't affect the construction of the
application itself.

Subject: What are the advantages to this approach?

Plan 9's approach improves generality and modularity of application design
by encouraging servers that make any kind of information appear to users and
to applications just like collections of ordinary files. Here are a few
examples.

The Plan 9 window system (called rio) is small and clean in part because its
design is centered on providing a virtual keyboard, mouse, and screen to
each of the applications running under it, while using the real keyboard,
mouse, and screen supplied by the operating system. That is - besides
creating, deleting, and arranging the windows themselves - its job is be a
server for certain resources used by its clients. As a side benefit, this
approach means that the window system can run recursively in one of its
windows, or even on another machine.

Plan 9 users do Internet FTP by starting a local program that makes all the
files on any FTP server (anywhere on the Internet) appear to be local files.
Plan 9 PC users with a DOS/Windows partition on their disk can use the files
stored there. ISO 9660 CD-ROMs and tar and cpio tapes all behave as if they
were native file systems. The complete I/O behavior and performance of any
application can be monitored by running it under a server that sees all its
interactions. The debugger can examine a program on another machine even if
it is running on a different hardware architecture.

Another example is the approach to networks. In Plan 9, each network
presents itself as a set of files for connection creation, I/O, and control.
A common semantic core for the operations is agreed upon, together with a
general server for translating human-readable addresses to network-specific
ones. As a result, applications don't care which kind of network (TCP/IP,
ISDN, modem) they are using. In fact, applications don't even know whether
the network they are using is physically attached to the machine the
application is running on: the network interface files can be imported from
another machine.

Hardware and Software:

Subject: What platforms does it run on?

The Plan 9 kernel and applications are highly portable. Plan 9 runs on four
major machine architectures: Intel 386/486/Pentium, MIPS, Alpha, and
PowerPC. Data structures and protocols are designed for distributed
computing on machines of diverse design. Except for necessarily
machine-dependent parts of the kernel, the compilers, and a few libraries,
there is a single source representation for everything.

To find out whether Plan 9 supports your hardware, read The Various Ports
and Supported PC Hardware.

Subject: Is anyone working on a port for my system?

Perhaps ... let us know.

Subject: Does it support symmetric multiprocessing?

Yes. The SGI Challenge series of multiprocessors and multi processor
Pentiums are supported. Be warned that Intel-based SMP systems are
notoriously fickle in conforming to the Multiprocessor Specification and
often some head-scratching is required when things don't just work.

The system has been run on machines ranging from dual Pentium 90's up to
quad Xeon 400's and the 8 processor Pentium Pro Axil system. By default, as
it comes out the box, the release has SMP operation disabled by an option in
the plan9.ini config file.

Subject: What about applications and tools?

Plan 9 comes with its own compilers for C and other languages, together with
all the commands and program-development tools originally pioneered in the
Unix environment. It also provides newly designed software. Acid is a
programmable debugger that understands multiple-process programs, and the
programs it is debugging may be running on a hardware platform different
from its own. Acme is a new user interface in which any word on the screen
can be interpreted as a command by clicking on it, and any string can
specify a file to be displayed.

Subject: Is there a fortran compiler?

No, plan9 does not have a fortran compiler. If you have fortran programs you
want to run, you can try using the f2c (fortran to C) converter available at
ftp://netlib.bell-labs.com/netlib/f2c/

Subject: Where can I get more Plan 9 software?

     Charles Forsyth has the original and still the longest list of software
          http://www.caldo.demon.co.uk/plan9/soft/index.html
     Russ Cox has cd players, mp3 player and a wide variety of other small
     tools
          http://www.eecs.harvard.edu/~rsc/plan9.html
     Tad Hunt can help you balance your bank account, boot your laptop and
     listen to music
          http://csh-east.org/~tad/plan9/
     Nemo (Francisco Ballesteros) has a collection of drivers and utilities
          http://plan9.escet.urjc.es/usr/nemo/9.html
     Kenji Arisawa's ftp site
          ftp://plan9.aichi-u.ac.jp/
     Boyd Roberts writes rc scripts when not ranting on 9fans
          http://home.fr.inter.net/boyd/code/repo/
     There's a native Python port at
          http://home.fr.inter.net/boyd/code/plan9/pythonR3.tgz [based on
          Russ' port to 9P2000]
     The wiki User-contributed Software page is useful too
          http://plan9.bell-labs.com/wiki/plan9/35/index.html

Subject: Is it object-oriented?

No, not in the conventional sense. It is written in a strict dialect of
ISO/ANSI C. In a wider sense, its general design of making all its `objects'
look like files to which one talks in a well-defined protocol shows a
related approach.

Subject: What about application portability?

Plan 9 comes with a library that makes it easy to import POSIX-conforming
applications. There is also a library that emulates the Berkeley socket
interface.

Subject: What resources does it need?

As might be expected, the answer depends on what you want to do. The kernel,
the window system, and the basic applications will run comfortably on a PC
with 8MB of memory.

On the other hand, the system can grow. The installation at Bell
Laboratories includes multiprocessor SGI Challenge and Pentium machines as
CPU servers, and a 350GB Sony WORM disk jukebox for the file server.

Subject: What GUIs does it support?

The standard interface doesn't use icons or drag-n-drop; Plan 9 people tend
to be text-oriented. But the window system, the editor, and the general feel
are very mousy, very point-and-click: Plan 9 windows are much more than a
bunch of glass TTYs. The system supports the graphics primitives and
libraries of basic software for building GUIs.
A screenshot is available at
http://plan9.bell-labs.com/plan9dist/screenshot.html

Subject: How do I cut and paste with a 2 button mouse?

Plan 9 really works well only with a three-button mouse. In the meantime,
Shift-Right-button will simulate a middle button, but that is inadequate for
Acme's chording.

Subject: Does Plan 9 have any Unix-like terminal emulators?

The Plan 9 window system doesn't obey any inline cursor controls, since none
of the native applications use cursor-addressing. All cursor control in rio,
acme and sam is via the mouse.

To see some excellent articles on this important and divisive user interface
issue read http://www.asktog.com/readerMail/1999-12ReaderMail.html.

If you want to get from Plan9 to Unix systems, you can run /bin/vt in one of
your windows, telnet/rlogin to Unix, and set the term/TERM variable
accordingly on the Unix end. See vt(1) for more details; note that vt(1) can
emulate a VT100 VT220 or ANSI terminal.

Subject: What character set does it use?

The character set is Unicode, the 16-bit set unified with the ISO 10646
standard for representing languages used throughout the world. The system
and its utilities support Unicode using a byte-stream representation (called
UTF-8) that is compatible with ASCII. On Plan 9, one may grep for Cyrillic
strings in a file with a Japanese name and see the results appear correctly
on the terminal.

Subject: What about security and user authentication?

Plan 9's authentication design is akin to that of MIT's Kerberos. Passwords
are never sent over networks; instead encrypted tickets are obtained from an
authentication server. It doesn't have the concept of `set UID' programs.
The file server doesn't run user programs, and except at its own console, it
doesn't allow access to protected files except by authenticated owners. The
concept of a special `root' user is gone.

Subject: How does it communicate with other systems?

The distribution includes a u9fs server that runs on Unix-compatible systems
and understands the native Plan 9 remote file protocol, so that file systems
of Unix machines may be imported into Plan 9. It also includes an
NFS-compatible server that runs on Plan 9, so that Plan 9 file systems may
be accessed from other systems that support NFS. It includes the full suite
of Internet protocols (telnet, rlogin, ftp).

Subject: Is it suitable for real time control?

No, it is not. It is a general purpose system, without an interrupt priority
scheme or real scheduler.

Installation and Administration:

Subject: What PC hardware works well with Plan 9?

If you don't want to spend time fiddling with and swapping PC hardware, you
may prefer to buy hardware that is in use within Bell Labs, see Supported PC
Hardware.

The biggest source of problems is getting the VGA configured on PC
terminals. For best performance and functionality, it is recommended that
you use a card that can run at 16 bits per pixel or greater, and with
hardware-accelerated graphics support; currently only the Mach64 and S3
Virge are supported to this extent. The best buy today seems to be the ATI
8Meg Xpert 98 cards.

For a cpuserver or fileserver any old card that can do CGA is fine.

Subject: How do I Install Plan 9?

The installation is designed to be run from a PC.
  1. Read "Supported PC hardware" to ensure your PC meets the requirements.
  2. Back up your system.
  3. Make sure you've backed up your system.
  4. Read "Installing the Plan 9 Distribution" at URL
     http://plan9.bell-labs.com/sys/doc/install.html
  5. Check the errata page at
     http://plan9.bell-labs.com/plan9dist/errata.html for problems and fixes
     found since the distribution was made.
  6. Here are some more questions that have been answered on the 9fans list:
     IP configuration
          ndb/cs will set the sysname if you setup an appropriate entry in
          /lib/ndb/local. You must specify an 'ether=' entry, and the
          address should be all lower case. If all goes well, ip/ipconfig
          will then configure IP.
     Name Service
          If you have having problems, first check that ndb/dns is running.
          It needs to be started in /rc/bin/termrc or /rc/bin/cpurc. Also
          note that only fully qualified names are supported, and there
          isn't a separate resolver.
     Binding and Mounting Devices
          Note that # is the shell comment character, so you must enclose it
          in single quotes. For example: bind -a '#R6' /dev
     Auth Server
          When booting a cpuserver without an auth server, if you give
          0.1.0.0 as the auth server address instead the cpu server's own
          address, you won't have to wait for it to timeout.

Subject: It doesn't work for me, how should I troubleshoot?

If you are having having SCSI problems, check your cables and terminators.
this is generally the single largest cause of weird SCSI problems. Active
terminators are best. If you run external cables you need to get high
quality ones. Also, don't crank of the speed on the card.

Subject: How do I setup the VGA?

If the VGA doesn't work, read the last couple sections (Setting Up and
Troubleshooting) of "Installing the Plan 9 Distribution"

You will have to find out more about the card so you can configure it. The
relevant manuals are: vga(3), vgadb(6), vga(8), and 9load(8).

If your VGA card is not supported, you could try
http://mapage.noos.fr/philippe.anel/ for Matrox G200 G400 and G450 drivers
by Philippe Anel or see http://plan9.bell-labs.com/wiki/plan9/39/index.html
for Riva TNT drivers by Nicholas Waples.

Put
debug=1
(1st line) in plan9.ini and try again. It may not be of much help but will
allow to ask a more specific question.

Subject: How do I control the services that start at boot time?

This is controlled by shell scripts, that are roughly equivalent to the
/etc/rc files on Unix:
/rc/bin/termrc for terminals
/rc/bin/cpurc for cpu servers

See cpurc(8) for more details.

Subject: How do I setup network services?

For UDP services, you must start them up in the appropriate cpurc(8) file.
For TCP or IL services, you must use the listen(8) daemon.

Subject: How do I shutdown my terminal/cpuserver system?

If you booted from a real fileserver, you can just turn it off.

If you are using kfs, you must halt the disks manually by typing
disk/kfscmd halt
at a prompt and waiting for ``kfs: file system halted'' to appear on the
screen.

Not doing this means the disk might not be in a consistent state or modified
data might not have been written out yet; not halting the disk forces the
long wait at the ``kfs...'' when you boot the next time while kfs checks the
disk.

Subject: How do I reboot my system?

The system can be rebooted by typing ^T^Tr (two control-T's followed by
'r'). Cpu servers can be rebooted by typing ^P on the console. See the
cons(3) manual for more details.

General Information:

Subject: Where did the name come from?

It was chosen in the Bell Labs tradition of selecting names that make
marketeers wince. The developers also wished to pay homage to the famous
film, "Plan 9 From Outer Space".

Subject: How can I Obtain Plan 9?

The Plan 9 release is available for free download at
http://plan9.bell-labs.com/plan9dist/download.html
It includes source of the kernel, libraries, and commands for all supported
architectures. It also includes complete binaries for the x86 architecture.

Subject: How can I get involved?

The best way to learn about the system is to write something that other
people in the Plan 9 user community could use, or to port the system to new
platforms.

Subject: Where can I get more detailed technical information?

The Bell Labs site plan9.bell-labs.com stores a wealth of information about
the system.
The manual pages are at http://plan9.bell-labs.com/sys/man/
For auxiliary documentation, see http://plan9.bell-labs.com/sys/doc/
A Plan 9 wiki is maintained by enthusiasts at
http://plan9.bell-labs.com/wiki/plan9/1/

Subject: Are there any Plan 9 user groups?

There is one in Austin, Texas. See
http://einstein.ssz.com/hangar18/index.html It's open to anyone, even if you
don't live in Austin.

Subject: Is the cross product of two vectors a vector?

No, it is not, and the fact that people treat it as one is the problem. The
*geometric object* that is the closest thing to the c.p. is a skew tensor
(practically the same as wedge product), which (only) in 3D has Cartesian
components that resemble those of a vector, *except* that this pseudo-vector
*flips* under reflection (unlike a genuine vector). Unfortunately,
physicists have been trained to express Maxwell's laws as a relationship
between a genuine vector (field) and a c.p., which means that that
expression of those laws *changes* under reflection, something that
physicists are *not* taught and which appears to have been overlooked in the
analysis of the (nonconservation of) parity experiment.
I had to quote Douglas Gwyn verbatim on this, because I have no *ucking clue
what he's talking about -- Steve

Subject: Can I emulate Plan 9 under Unix?

Ron Minnich has implemented private name spaces for Linux and FreeBSD. You
can get documentation and source code from
http://www.acl.lanl.gov/~rminnich/

Several Plan 9 inspired applications are available for Unix systems.

The sam editor is available from
ftp://netlib.bell-labs.com/netlib/research/sam.shar.gz

There is also a Windows 95/NT version of Sam, currently distributed in
binary form only, available from
ftp://netlib.bell-labs.com/netlib/research/sam.exe Comments and bug reports
can be sent to seanq@research.bell-labs.com

Wily is an acme lookalike by Gary Capell. See
http://www.cs.yorku.ca/~oz/wily/

Mark H. Wilkinson's 9libs package of Plan 9 emulation libraries for Unix is
probably the easiest-to-install distribution of sam and wily. You can get it
from http://www.netlib.org/research/9libs/

A free re-implementation of the rc shell is available from
http://www.star.le.ac.uk/~tjg/rc/

9wm is David Hogan's lightweight X window manager in the style of 8�/rio. It
was once available from ftp://ftp.cs.su.oz.au/dhog/9wm/ but is being moved
to a new home. Comments to dhog@plan9.bell-labs.com

9term is an 8� terminal emulator by Matty Farrow, matty@cs.su.oz.au,
available from ftp://ftp.cs.su.oz.au/matty/unicode/ In the same directory,
you'll find a collection of Unicode fonts that can be used with 9term, sam
and wily.

9menu is a simple program by Arnold Robbins, arnold@skeeve.com, that allows
you to create X menus from the shell, where each menu item will run a
command. 9menu is intended for use with 9wm, but can be used with any other
window manager. It is available from
ftp://ftp.freefriends.org/arnold/Source/9menu-1.5.shar.gz

Copyright � 1995 Lucent Technologies. All rights reserved.
-- 
Steve Kotsopoulos		steve@fywss.com
